<?php

namespace ADW\RestClientBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use ADW\RestClientBundle\DependencyInjection\Compiler\ClientsCompiler;

/**
 * Class ADWRestClientBundle.
 *
 * @author Artur Vesker
 */
class ADWRestClientBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
    }
}
