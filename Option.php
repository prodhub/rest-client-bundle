<?php

namespace ADW\RestClientBundle;

/**
 * Class Option.
 *
 * @author Artur Vesker
 */
class Option
{
    /**
     * @var string[]
     */
    protected $types;

    /**
     * @var bool
     */
    protected $required;

    /**
     * @var mixed
     */
    protected $defaultValue;

    /**
     * @var null|array
     */
    protected $allowedValues;

    /**
     * @param array|string $type
     * @param bool         $required
     * @param null|mixed   $defaultValue
     * @param array        $allowedValues
     */
    public function __construct($type, $required = false, $defaultValue = null, $allowedValues = null)
    {
        $this->types = is_string($type) ? [$type] : $type;
        $this->types = array_merge(['null'], $this->types);
        $this->required = $required;
        $this->defaultValue = $defaultValue;
        $this->allowedValues = $allowedValues;
    }

    /**
     * @return string[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @return null|array
     */
    public function getAllowedValues()
    {
        return $this->allowedValues;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }
}
