<?php

namespace ADW\RestClientBundle\Client;

use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;
use ADW\RestClientBundle\Event\ExceptionEvent;
use ADW\RestClientBundle\Event\RequestEvent;
use ADW\RestClientBundle\Event\ResponseEvent;
use ADW\RestClientBundle\Option;

/**
 * Class Client.
 *
 * @author Artur Vesker
 */
class Client
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $guzzle;

    /**
     * @var \ADW\RestClientBundle\Client\ClientDescriptionInterface
     */
    protected $clientDescription;

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    protected $eventDispatcher;

    /**
     * @param Guzzle $guzzle
     * @param ClientDescriptionInterface $clientDescription
     */
    public function __construct(Guzzle $guzzle, ClientDescriptionInterface $clientDescription, EventDispatcherInterface $eventDispatcher = null)
    {
        $this->guzzle = $guzzle;
        $this->clientDescription = $clientDescription;
        $this->eventDispatcher = $eventDispatcher ?: new EventDispatcher();

        $this->attachListeners($this->eventDispatcher, $clientDescription->getSubscribedEvents());
    }

    /**
     * @return Guzzle
     */
    public function getGuzzle()
    {
        return $this->guzzle;
    }

    /**
     * @param Guzzle $guzzle
     *
     * @return self
     */
    public function setGuzzle($guzzle)
    {
        $this->guzzle = $guzzle;

        return $this;
    }

    /**
     * @return ClientDescriptionInterface
     */
    public function getClientDescription()
    {
        return $this->clientDescription;
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcher
     */
    public function getEventDispatcher()
    {
        return $this->eventDispatcher;
    }

    /**
     * @param string $rawPath
     * @param array $query
     * @param array $options
     *
     * @return Uri
     *
     * @throws \Exception
     */
    protected function resolveUri($rawPath, array $query, array $options)
    {
        $query = array_filter($query);
        $matches = [];
        preg_match_all('/[\{]{1}([\w]+)[\}]{1}/', $rawPath, $matches);
        $requiredOptions = $matches[1];

        foreach ($requiredOptions as $option) {
            if (!array_key_exists($option, $options)) {
                throw new \Exception('Missing option ' . $option);
            }
            $rawPath = str_replace('{' . $option . '}', $options[$option], $rawPath);
        }

        $uri = Uri::fromParts(
            [
                'scheme' => $this->clientDescription->getSchema(),
                'host' => $this->clientDescription->getHost(),
                'path' => $rawPath,
            ]
        );

        foreach ($query as $name => $value) {
            $uri = Uri::withQueryValue($uri, $name, $value);
        }

        return $uri;
    }


    /**
     * @param MethodDescriptionInterface $description
     * @param $data
     * @param $format
     * @param $type
     * @param array $context
     */
    protected function resolveResponseBody(MethodDescriptionInterface $description, $data, $format, $type, array $context)
    {
        $deserializer = $this->clientDescription->getDeserializer();

        if ($format == MethodDescriptionInterface::FORMAT_JSON) {
            return $deserializer($description, $data, $format, $type, $context);
        };

        if ($format == MethodDescriptionInterface::FORMAT_XML) {
            return $deserializer($description, $data, $format, $type, $context);
        };

        return;
    }

    /**
     * @param MethodDescriptionInterface $methodDescription
     * @param RequestInterface $request
     * @param ResponseInterface $response
     *
     * @param array $options
     * @return mixed
     */
    protected function handleResponse(MethodDescriptionInterface $methodDescription, RequestInterface $request, ResponseInterface $response, array $options)
    {
        return $this->resolveResponseBody(
            $methodDescription,
            (string)$response->getBody(),
            $methodDescription->getResponseDataFormat(),
            $methodDescription->getResponseDataModel(),
            array_merge($methodDescription->getResponseDataContext(), ['_request' => $request, '_response' => $response])
        );
    }

    /**
     * @param MethodDescriptionInterface $methodDescription
     * @param array $options
     *
     * @return $this|\GuzzleHttp\Psr7\MessageTrait|Request
     */
    public function createRequest(MethodDescriptionInterface $methodDescription, array $options = [])
    {
        $headers = [];
        $body = null;
        $method = $methodDescription->getMethod();

        if (in_array($method, ['POST', 'PATCH', 'PUT', 'DELETE'])) {
            if ($data = $methodDescription->getRequestData($options)) {
                $format = $methodDescription->getRequestDataFormat();
                switch ($format) {
                    case MethodDescriptionInterface::FORMAT_JSON:
                        $serializer = $this->clientDescription->getSerializer();

                        $body = \GuzzleHttp\Psr7\stream_for($serializer($data, $format, $methodDescription->getRequestDataContext()));
                        $headers['Content-Type'] = 'application/json';

                        break;
                    case MethodDescriptionInterface::FORMAT_XML:
                        $serializer = $this->clientDescription->getSerializer();
                        $body = \GuzzleHttp\Psr7\stream_for($serializer($data, $format, $methodDescription->getRequestDataContext()));
                        $headers['Content-Type'] = 'application/xml';

                        break;
                    case MethodDescriptionInterface::FORMAT_MULTIPART_FORM_DATA:
                        $elements = [];
                        foreach ($data as $name => $value) {
                            if ($value instanceof File) {
                                $value = fopen($value->getRealPath(), 'r');
                            }
                            $elements[] = ['name' => $name, 'contents' => $value];
                        }
                        $body = new MultipartStream($elements);
                        $headers['Content-Type'] = 'multipart/form-data; boundary=' . $body->getBoundary();
                        break;
                    case MethodDescriptionInterface::FORMAT_URLENCODED:
                        //TODO work for me (not tested)
                        $body=http_build_query($data);
                        $headers['Content-Type'] = 'application/x-www-form-urlencoded';
                }
            }
        }

        $request = new Request(
            $methodDescription->getMethod(),
            $this->resolveUri($methodDescription->getResource(), $methodDescription->getQuery($options), $options),
            $headers,
            $body
        );

        return $request;
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     * @param $listeners
     */
    protected function attachListeners(EventDispatcherInterface $eventDispatcher, $listeners)
    {
        foreach ($listeners as $listener) {
            $eventDispatcher->addListener($listener['event'], $listener['listener']);
        }
    }

    /**
     * @param MethodDescriptionInterface $methodDescription
     * @param array $options
     *
     * @return mixed
     */
    public function request(MethodDescriptionInterface $methodDescription, array $options = [])
    {
        $options = $this->resolveMethodOptions($methodDescription, $options);

        $eventDispatcher = clone $this->eventDispatcher;
        $this->attachListeners($eventDispatcher, $methodDescription->getSubscribedEvents());

        $requestEvent =  new RequestEvent($this->createRequest($methodDescription, $options), $methodDescription, $options);
        $eventDispatcher->dispatch(RequestEvent::NAME, $requestEvent);
        $request = $requestEvent->getRequest();

        try {
            $response = $this->guzzle->send($request);
        } catch (RequestException $e) {
            $this->eventDispatcher->dispatch(ExceptionEvent::NAME, new ExceptionEvent($e, $methodDescription, $options));
            throw $e;
        }

        $eventDispatcher->dispatch(ResponseEvent::NAME, new ResponseEvent($request, $response, $methodDescription, $options));

        return $this->handleResponse($methodDescription, $request, $response, $options);
    }


    /**
     * @param MethodDescriptionInterface $description
     * @param array $options
     *
     * @return array
     */
    private function resolveMethodOptions(MethodDescriptionInterface $description, array $options)
    {
        $optionsResolver = new OptionsResolver();

        $definedOptions = $description->getOptions();

        foreach ($definedOptions as $name => $option) {
            if ($option instanceof Option) {
                if ($option->isRequired()) {
                    $optionsResolver->setRequired($name);
                }
                $optionsResolver->setDefault($name, $option->getDefaultValue());
                $optionsResolver->setAllowedTypes($name, $option->getTypes());

                if ($option->getAllowedValues()) {
                    $optionsResolver->setAllowedValues($name, $option->getAllowedValues());
                }
            } elseif (is_string($option) || is_array($option)) {
                $optionsResolver->setRequired($name);
                $optionsResolver->setAllowedTypes($name, $option);
            }
        }

        return $optionsResolver->resolve($options);
    }

}
