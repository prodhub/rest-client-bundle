<?php

namespace ADW\RestClientBundle\Client;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Interface ClientDescriptionInterface.
 *
 * @author Artur Vesker
 */
interface ClientDescriptionInterface
{
    /**
     * @return string
     */
    public function getHost();

    /**
     * @return string
     */
    public function getSchema();

    /**
     * @return callable
     */
    public function getSerializer();

    /**
     * @return callable
     */
    public function getDeserializer();

    /**
     * @return array
     */
    public function getSubscribedEvents();

}
