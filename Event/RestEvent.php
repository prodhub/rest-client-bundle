<?php

namespace ADW\RestClientBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class RestEvent
 *
 * @package ADW\RestClientBundle\Event
 * @author Artur Vesker
 */
class RestEvent extends Event
{

    /**
     * @var MethodDescriptionInterface
     */
    protected $methodDescription;

    /**
     * @var array
     */
    protected $options;

    /**
     * @param MethodDescriptionInterface $methodDescription
     * @param array $options
     */
    public function __construct(MethodDescriptionInterface $methodDescription, array $options)
    {
        $this->methodDescription = $methodDescription;
        $this->options = $options;
    }

    /**
     * @return MethodDescriptionInterface
     */
    public function getMethodDescription()
    {
        return $this->methodDescription;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }



}
