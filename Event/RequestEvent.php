<?php

namespace ADW\RestClientBundle\Event;

use Psr\Http\Message\RequestInterface;
use Symfony\Component\EventDispatcher\Event;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class RequestEvent.
 *
 * @author Artur Vesker
 */
class RequestEvent extends RestEvent
{
    const NAME = 'rest_client.request';

    /**
     * @var RequestInterface
     */
    protected $request;

    public function __construct(RequestInterface $request, MethodDescriptionInterface $methodDescription, array $options = [])
    {
        $this->request = $request;
        $this->methodDescription = $methodDescription;
        $this->options = $options;

        parent::__construct($methodDescription, $options);
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param RequestInterface $request
     *
     * @return self
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }
}
