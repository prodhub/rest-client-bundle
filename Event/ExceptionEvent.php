<?php

namespace ADW\RestClientBundle\Event;

use GuzzleHttp\Exception\RequestException;
use Symfony\Component\EventDispatcher\Event;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class ExceptionEvent.
 *
 * @author Artur Vesker
 */
class ExceptionEvent extends RestEvent
{
    const NAME = 'rest.exception';

    /**
     * @var RequestException
     */
    protected $exception;

    /**
     * @param RequestException           $exception
     * @param MethodDescriptionInterface $methodDescription
     * @param array                      $options
     */
    public function __construct(RequestException $exception, MethodDescriptionInterface $methodDescription, array $options)
    {
        $this->exception = $exception;
        $this->methodDescription = $methodDescription;
        $this->options = $options;
        parent::__construct($methodDescription, $options);
    }

    /**
     * @return RequestException
     */
    public function getException()
    {
        return $this->exception;
    }

}
