<?php

namespace ADW\RestClientBundle\Event;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\EventDispatcher\Event;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class ResponseEvent.
 *
 * @author Artur Vesker
 */
class ResponseEvent extends RestEvent
{

    const NAME = 'rest_client.response';

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param MethodDescriptionInterface $methodDescription
     * @param array $options
     */
    public function __construct(RequestInterface $request, ResponseInterface $response, MethodDescriptionInterface $methodDescription, array $options = [])
    {
        $this->request = $request;
        $this->response = $response;
        parent::__construct($methodDescription, $options);
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }
}
