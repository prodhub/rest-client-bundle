# RestClientBundle

## Installation
```
#!bash
composer require adw/rest-client-bundle

```
##### AppKernel.php
```
public function registerBundles()
{
  $bundles = [
    ...
     new ADW\RestClientBundle\ADWRestClientBundle(),
     new ADW\GuzzleBundle\ADWGuzzleBundle(),
  ]
}
```

##### composer.json
```
#!json
"repositories": [
   { "type": "git", "url": "https://bitbucket.org/prodhub/rest-client-bundle.git" },
   { "type": "git", "url": "https://bitbucket.org/prodhub/guzzle-bundle.git" }
]
```

## Usage

//TODO
