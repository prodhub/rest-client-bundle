<?php

namespace ADW\RestClientBundle\Description;

/**
 * Interface DescriptionInterface.
 *
 * @author Artur Vesker
 */
interface MethodDescriptionInterface
{
    const FORMAT_JSON = 'json';
    const FORMAT_XML = 'xml';
    const FORMAT_URLENCODED = 'urlencoded';
    const FORMAT_MULTIPART_FORM_DATA = 'multipart_form_data';

    /**
     * {@inheritdoc}
     */
    public function getResponseDataFormat();

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel();

    /**
     * {@inheritdoc}
     */
    public function getResponseDataContext();

    /**
     * {@inheritdoc}
     */
    public function getOptions();

    /**
     * {@inheritdoc}
     */
    public function getResource();

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options);

    /**
     * {@inheritdoc}
     */
    public function getMethod();

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options);

    /**
     * {@inheritdoc}
     */
    public function getRequestDataFormat();

    /**
     * {@inheritdoc}
     */
    public function getRequestDataContext();

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents();
}
